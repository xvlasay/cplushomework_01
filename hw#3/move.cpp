#include<iostream> 
#include <cstring>
#include <iomanip>      // std::setprecision
using namespace std;

class Move
{
private:
    double x;
    double y;
public:
    Move(double a = 0, double b = 0);   // sets x, y to a, b
    void showmove() const  {             // shows current x, y values
	std::cout << "showmove:" << endl;
	std::cout << std::setprecision(2) << fixed << "x = " <<  x << "; y = "  << y << endl;
     }

//    Move add(const Move & m) const;
// this function adds x of m to x of invoking object to get new x,
// adds y of m to y of invoking object to get new y, creates a new
// move object initialized to new x, y values and returns it
    Move add(const Move & m) const {
	double x1,y1;
	x1 = this->x + m.x;
	y1 = this->y + m.y;
	Move mv2(x1,y1);
	return mv2;
    }
    void reset(double a = 0.0, double b = 0.0) { // resets x,y to a, b
	x = a;
	y = b;
    }
   
};
Move::Move(double a, double b) {
	x = a; y = b;
}

int main () {
	Move mv(1.0, 2.0);
	mv.showmove();
	Move mv1(10.0, 20.0);
	mv1.showmove();
	Move mv2= mv.add(mv1);
	mv2.showmove();
	cout << "Reset:" << endl;
	mv.reset();
	mv.showmove();
	return 0;
}


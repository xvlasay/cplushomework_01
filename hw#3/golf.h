// golf.h -- for pe9-1.cpp

constexpr int Len = 40;
struct golf
{
    char fullname[Len];
    int handicap;
};


#include<iostream> 
#include <cstring>
using namespace std;
class Person {
private:
static constexpr int LIMIT = 25;
string mLastName; // Person's last name
char mFirstName[LIMIT]; // Person's first name
public:
Person() {
mLastName = ""; 
mFirstName[0] = '\0'; 
} // #1
Person(const string & ln, const char * fn = "Heyyou"); // #2
void SetFirstName (char *FirstName) {
        strcpy( this->mFirstName, FirstName);
}
void SetLastName (string LastName) {
         this->mLastName = LastName;
}
// the following methods display mLastName and mFirstName
//void Show() const; // firstname lastname format
//void FormalShow() const; // lastname, firstname format
void Show() const {
	cout<< "Show: "  << endl;
        cout<< "First name: " << mFirstName << endl;
        cout << "Last name: " << mLastName << endl;
}
void FormalShow() const {
	cout<< "FormalShow: "  << endl;
        cout << "Last name: " << mLastName << endl;
        cout<< "First name: " << mFirstName << endl;
}

};
int main() {
	Person pers;
	pers.SetFirstName((char*) "John");
	pers.SetLastName("Smith");
	pers.Show();
	pers.FormalShow();
	return 0;
}	


